ql_infra_app_qlgateway
==================

Used to manage application 'qlgateway' (formally known as 'qlpos'.  Manages application deployment via ECS and all related AWS infrastructure.

This application role is used twice:

 - By default the type is 'qlgateway-api'.
 - The Jenkinsfile in qlgateway is configured to override the type to 'qlgateway-console'. This is so that traffic for
 the console and API is kept separate.

Requirements
------------

ECS cluster needs to exist
RDS instance for DB needs to exist

Role Variables
--------------

create_or_destroy - whether to create or destory the application infrastructure
docker_tag_to_use - docker tag to deploy
type - application type
ecs_repo_prefix - ECS repository prefix
ecs_repo_name - ECS repository name
cluster_id - ECS cluster ID
desired_count - desired number of containers
container_port - internal container port used by application
aws_rds_instance_user - RDS instance username
aws_rds_instance_password - RDS instance password
db_name - DB name used by application
service_dns_name - DNS name of the service (to be used by Nginx)
set_dns - whether to set the DNS name above when running the create infrastructure play (you might want to do this later if you need to test before Nginx updates)
allow_against_prod - allow the infrastructure role to be run against prd environment

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

To deploy a particular version of the app you should pass the docker_tag_to_use variable.  Otherwise this will default to 'latest'

    - hosts: local
      roles:
         - { role: ql_infra_aws_app_qlpos, docker_tag_to_use: 1.2.3 }

License
-------

Commercial

Author Information
------------------

Rob White (robert.white@qantasloyalty.com.au)
